#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <signal.h>
#include <ctype.h>

#define MAX_LEN 512
#define MAXARGS 10
#define ARGLEN 30
#define PROMPT "PUCIT@shell:"
#define MAXHIST 10
#define MAXBUILTINCMDS 5

int execute(char* arglist[], char redirection[3][512]);
char ** tokenize(char * cmdline, char redirection[3][512]);
char * read_cmd(char* , FILE*);
void redirect (char redirection[3][512]);
void resetRedirection();
void pipedExecution(char* cmdline, char redirection[3][512]);
char ** getComand(char* cmdline, char * toBeRemoved);
void executeMultipleCmds(char * cmdline, char redirection[3][512]);
void trim(char* input);
void callBuiltin(char** arglist);
void showHelp();
void changedir(char ** arglist);
void killPid(char ** arglist);

int inpRed=0, outRed=0, errRed=0;
int noOfPipes=0;
int multipleCmds=0;
int isBg=0;
int isBuiltin=0;
int totalNoOfCmds=0;

//creating array of built-in commands
char builtin[MAXBUILTINCMDS][512]={"cd", "exit", "jobs", "kill", "help"};
char syntaxOfBuiltin[MAXBUILTINCMDS][512]= {"cd [dir name]", "exit", "jobs", "kill [pid]", "help"};

int main()
{
	char* cmdline;
	char ** arglist;

	//creating history array
	char ** history;
	history = (char **) malloc (sizeof(char*) * MAXHIST);
	for (int i=0; i< MAXHIST; i++)
	{
		history[i]= (char *) malloc (sizeof(char)*512);
		bzero(history[i], 512);
	}

	
	char redirection[3][512];

	char prompt[512]=PROMPT;
	char cwd[512];
	getcwd(cwd, sizeof(cwd));

	signal(SIGINT, SIG_IGN);
	
	while ((cmdline= read_cmd(prompt, stdin)) != NULL)
	{
		//populate history array with cmdline

		if (cmdline[0]!='!')
		{
			strcpy(history[totalNoOfCmds%MAXHIST], cmdline);
			totalNoOfCmds++;
		}

		//If entered command needs history i.e has an !
		if (cmdline[0]=='!')
		{
			int cmdNum;
			if (cmdline[1]=='-')
			{
				cmdNum= cmdline[2]-'0';
				cmdNum= cmdNum*-1;
				cmdNum= (totalNoOfCmds%MAXHIST) + cmdNum +1;
			}
			else
			{
				cmdNum= cmdline[1]-'0';
			}
			strcpy(cmdline, history[cmdNum-1]);
		}


		if ((arglist = tokenize(cmdline, redirection))!= NULL)
		{
			callBuiltin(arglist);
			if (isBuiltin==1)
			{
				isBuiltin=0;
				continue;
			}
			if (noOfPipes!=0)
				pipedExecution(cmdline, redirection);
			else if (multipleCmds!=0)
				executeMultipleCmds(cmdline, redirection);
			else
				execute(arglist, redirection);
		
			//need to free arglist
			for (int j=0; j<MAXARGS+1; j++)
			{
				free(arglist[j]);
			}
			free(arglist);
			free(cmdline);
		}
	} //end of while loop
	printf("\n");

	//Store history
	FILE* hist= fopen("history.txt", "w");
	for (int n=0; n<MAXHIST; n++)
	{
		fprintf(hist, history[n]);
		fprintf(hist, "\n");
	}
	return 0;
}

int execute(char * arglist[], char redirection[3][512])
{
	int status;
	int cpid= fork();
	
	switch(cpid)
	{
		case -1:
			perror("fork failed");
			exit(1);
		case 0:
			signal(SIGINT,SIG_DFL);
			if (isBg==1)
			{
				setpgid(cpid,0);
			}
			if (inpRed==1 || outRed==1 || errRed==1)
			{
				redirect(redirection);
			}
			
			execvp(arglist[0], arglist);
			perror("command not found");
			exit(1);
		default:
			if (isBg)
				signal(SIGCHLD, SIG_IGN);
			if (isBg!=1)
			{
				waitpid(cpid, &status, 0);
			}
			isBg=0;
			if (inpRed==1 || outRed==1 || errRed==1)
			{
				resetRedirection();
			}
			return 0;
	}
}

char** tokenize(char* cmdline, char redirection[3][512])
{
	//allocate memory
	char** arglist= (char**)malloc(sizeof(char*)*(MAXARGS+1));
	for (int i=0; i<MAXARGS+1; i++)
	{
		arglist[i]= (char*)malloc(sizeof(char)* ARGLEN);
		bzero(arglist[i], ARGLEN);
	}
	
	if (cmdline[0]== '\0') //if user has entered nothing and pressed enter
		return NULL;
	int argnum=0; //slots used
	char * cp = cmdline; //pos in string
	char * start;
	int len;

	int isRedirected=-1;

	while (*cp != '\0')
	{
		while (*cp == ' ' || *cp== '\t') //skip leading spaces
		{
			cp++;
		}
		start= cp; //start of the word
		len=1;
		//check if we have a redirection symbol

		if (*cp=='<')
		{
			isRedirected=0;
			inpRed=1;
			cp++;
			continue;
		}
		else if (*cp=='>')
		{
			isRedirected=1;
			outRed=1;
			cp++;
			continue;
		}
		else if (*cp=='2')
		{
			if ((*(cp+1))=='>' && (*((cp+2))==' ' || (*(cp+2))=='\t'))
			{
				isRedirected=2;
				errRed=1;
				cp=cp+2;
				continue;
			}
		}
		else if (*cp=='|')
		{

			noOfPipes++;
		}
		else if (*cp== ';')
		{
			multipleCmds++;
		}

		//find the end of the word 

		while (*++cp != '\0' && !(*cp==' ' || *cp=='\t'))
		{
			len++;
		}

		if (isRedirected!=-1)
		{
			strncpy(redirection[isRedirected],start,len);
			isRedirected=-1;
		}
		else
		{
			strncpy(arglist[argnum], start, len);
			arglist[argnum][argnum] != '\0';
			argnum++;
		}
	}
	if (!(strcmp(arglist[argnum-1], "&")))
	{
		isBg=1;
		arglist[argnum-1]=NULL;
	}
	else
	{
		arglist[argnum]= NULL;
	}
	return arglist;
}

char * read_cmd(char* prompt, FILE* fp)
{
	char cwd[512];
	getcwd(cwd, sizeof(cwd));

	printf("%s%s# ", prompt,cwd);
	int c;
	int pos=0; //position of character
	char* cmdline= (char*)malloc(sizeof(char)*MAX_LEN);
	while((c = getc(fp))!= EOF)
	{
		if (c=='\n')
			break;
		cmdline[pos++]=c;
	}
	//these two lines are added, in case user presses ctrl+D to exit shell
	if (c== EOF && pos==0)
		return NULL;
	cmdline[pos]='\0';
	return cmdline;
}


void redirect (char redirection[3][512])
{
if (inpRed==1)
			{
				int fdR= open(redirection[0], O_RDONLY);
				int newfdR= dup2(fdR,0);
				close(fdR);
			}
			if (outRed==1)
			{
				int fdW= open(redirection[1], O_WRONLY | O_CREAT);
				int newfdW= dup2(fdW,1);
				close(fdW);
			}
			if (errRed==1)
			{
				int fdE= open(redirection[2], O_WRONLY | O_CREAT);
				int newfdE= dup2(fdE,2);
				close(fdE);
			}
}

void resetRedirection ()
{
if (inpRed==1)
			{
				close(0);
				fopen("/dev/tty", "r");
				inpRed=0;
			}
			if (outRed==1)
			{
				close(1);
				fopen("/dev/tty", "w");
				outRed=0;
			}
			if (errRed==1)
			{
				close(2);
				fopen("/dev/tty","w");
				errRed=0;
			}
}

char** getCommand(char* cmdline, char * toBeRemoved)
{
	char** commands= (char**)malloc(sizeof(char*)*(MAXARGS+1));
	for (int i=0; i<MAXARGS+1; i++)
	{
		commands[i]= (char*)malloc(sizeof(char)* ARGLEN);
		bzero(commands[i], ARGLEN);
	}

	char * token = strtok(cmdline, toBeRemoved);

	int i=0;
	while (token!=NULL)
	{
		commands[i]=token;
		trim(commands[i]);
		token= strtok(NULL, toBeRemoved);
		i++;
	}

	commands[i]=NULL;

	return commands;
}

void trim(char * input)
{
	int i;
	int begin=0;
	int end=strlen(input)-1;
	while (isspace((unsigned char) input[begin]))
	{
		begin++;
	}
	while ((end>= begin) && isspace((unsigned char) input[end]))
	{
		end--;
	}
	for (i=begin; i<=end; i++)
	{
		input[i-begin]= input[i];
	}
	input[i-begin]='\0';

}

void pipedExecution(char* cmdline, char redirection[3][512])
{
	int pipes[2* noOfPipes];

	for (int i=0; i<noOfPipes; i++)
	{
		if (pipe(pipes + i*2) <0)
		{
			perror("cant pipe");
			exit(0);
		}
	}

	//getting tokenized commands
	
	char ** cmds= getCommand(cmdline, "|");
	char ** args;

	int i=0;
	int j=0;
	while (cmds[i] != NULL)
	{

		if (inpRed==1 || outRed==1 || errRed==1)
		{
			resetRedirection();
		}

		args= tokenize(cmds[i], redirection);
		pid_t cpid= fork();
		if (cpid==0)
		{
			signal(SIGINT,SIG_DFL);
			if (cmds[i+1] != NULL)
			{
				if (dup2(pipes[j+ 1], 1) < 0)
				{
					perror("dup2");
					exit(0);
				}
			}

			if (j !=0)
			{
				if (dup2(pipes[j-2], 0) < 0)
				{
					perror("dup2");
					exit(0);
				}
			}

			for (int l=0; l< 2*noOfPipes; l++)
			{
				close(pipes[l]);
			}

			if (inpRed==1 || outRed==1 || errRed==1)
			{
				redirect(redirection);
			}

			if ( execvp(args[0], args) <0)
			{
				perror("error in exec");
				exit(0);
			}
		}
		else if (cpid < 0)
		{
			perror("fork fail");
			exit(0);
		}
		i++;
		j+=2;
	}


	for (int k=0; k< 2*noOfPipes; k++)
	{
		close(pipes[k]);
	}

	for (int k=0; k< noOfPipes+1; k++)
	{
		wait(NULL);
	}

	if (inpRed==1 || outRed==1 || errRed==1)
	{
		resetRedirection();
	}

}

void executeMultipleCmds(char * cmdline, char redirection[3][512])
{
	char ** cmds= getCommand(cmdline, ";");
	char ** args;

	int i=0;
	while (cmds[i]!=NULL)
	{
		args= tokenize(cmds[i], redirection);
		int j=0;
		execute (args, redirection);
		i++;
	}

}

void callBuiltin(char ** arglist)
{
	if (!(strcmp(arglist[0], builtin[0])))
	{
		changedir(arglist);
		isBuiltin=1;
		return;
	}
	else if (!(strcmp(arglist[0], builtin[1])))
	{
		exit(0);
	}
	else if (!(strcmp(arglist[0], builtin[3])))
	{
		killPid(arglist);
		isBuiltin=1;
		return;

	}
	else if (!(strcmp(arglist[0], builtin[4])))
	{
		showHelp();
		isBuiltin=1;
		return;
	}


}

void changedir(char** arglist)
{
	chdir(arglist[1]);
}

void showHelp()
{
	for (int i=0; i< MAXBUILTINCMDS; i++)
	{
		printf("%s\t%s\n", builtin[i], syntaxOfBuiltin[i]);
	}
}

void killPid(char ** arglist)
{
	int pid= atoi(arglist[1]);
	kill(pid, SIGKILL);
}
